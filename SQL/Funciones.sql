create function public.sp_loguser(p1 varchar, p2 varchar)
 returns table (idempleado varchar(50), usuario varchar(16), contra varchar(32), nombre varchar(100), idpuesto integer, descripcion varchar(100))
as $$
begin
	return query
	select a.id_empleado, a.usuario, a.contra, b.nombre, b.id_puesto, c.descripcion from usuarios as a
	inner join empleados as b on a.id_empleado = b.id
	inner join puestos as c on b.id_puesto = c.id
	where a.id_empleado = p1 and a.contra = p2;
end;
$$ language plpgsql;
select * from sp_loguser('02069003077420181118', '1234');
-------------------------------------------------------------------------------------------------------------
create function public.sp_cargar_maquinas()
 returns table (id varchar(10))
as $$
begin
	return query
	select a.id from maquinas as a;
end;
$$ language plpgsql;
select * from sp_cargar_maquinas();
-------------------------------------------------------------------------------------------------------------
create function public.sp_cargar_rutas()
 returns table (id integer)
as $$
begin
	return query
	select a.id from rutas as a order by a.id asc;
end;
$$ language plpgsql;
select * from sp_cargar_rutas();
-------------------------------------------------------------------------------------------------------------
create function public.sp_existe_terminal(p1 varchar)
 returns table (id varchar(50))
as $$
begin
	return query
	select a.id from terminales as a where a.id = p1;
end;
$$ language plpgsql;
select * from sp_existe_terminal('C600000336');
-------------------------------------------------------------------------------------------------------------
create function public.sp_existe_tara(p1 varchar)
 returns table (id varchar(50))
as $$
begin
	return query
	select a.id from taras as a where a.id = p1;
end;
$$ language plpgsql;
select * from sp_existe_tara('2107107342106');
-------------------------------------------------------------------------------------------------------------
create function public.sp_existe_empleado(p1 varchar)
 returns table (id varchar(50))
as $$
begin
	return query
	select a.id from empleados as a where a.id = p1;
end;
$$ language plpgsql;
select * from sp_existe_empleado('02069003077420181118');
-------------------------------------------------------------------------------------------------------------
