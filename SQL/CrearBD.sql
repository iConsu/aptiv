create database "Aptiv"
    with
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    CONNECTION limit = -1;
