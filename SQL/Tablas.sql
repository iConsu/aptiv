-------------------------------------------------------------------------------------------------------
--TABLA QUE INDICA EL ESTADO EN EL QUE SE ENCUENTRA "X" REGISTRO
create table estados(id integer not null primary key
,descripcion varchar(20) not null
,abreviado char(1) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LAS RUTAS QUE PUEDE ELEGIR UNA TERMINAL QUE VA A CORTE
create table rutas(id serial not null primary key
,descripcion varchar(20) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LOS DIFERENTES CONTENEDORES PARA LOS TERMINALES
create table contenedores(id serial not null primary key
,abreviado varchar(5) not null
,descripcion varchar(20) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LOS PUESTOS QUE OCUPAN LOS EMPLEADOS EN LA EMPRESA
create table puestos(id serial not null primary key
,descripcion varchar(100) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LAS UNIDADES DE MEDIDAS
create table medidas(id serial not null primary key
,descripcion varchar(50) not null
,abreviado varchar(5) null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLAS PARA LOCALIZAR LA UBICACION DE LAS TERMINALES
create table localizacion(id serial not null primary key
,rack varchar(5) not null
,seccion smallint not null
,nivel smallint not null
,posicion varchar(5) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
create table maquinas(id varchar(10) not null primary key
,descripcion varchar(50) null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA ENLISTAR LOS EMPLEADOS DE LA EMPRESA
create table empleados(id varchar(50) not null primary key
,nombre varchar(100) not null
,id_puesto integer references puestos(id) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA USUARIOS CON PERMISOS DE INICIO DE SESION EN EL SISTEMA
create table usuarios(id serial not null primary key
,id_empleado varchar(50) references empleados(id) not null
,usuario varchar(16) not null
,contra varchar(32) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LA LISTA DE TERMINALES CON SU INFORMACION
create table terminales(id varchar(50) not null primary key
,id_medida integer references medidas(id) not null
,id_localizacion integer references localizacion(id) not null
,descripcion varchar(100) not null
,peso numeric(18,4) not null
,peso_unidad numeric(18,4) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLAS PARA LA LISTA DE TARAS CON SU INFORMACION
create table taras(id varchar(50) not null primary key
,descripcion varchar(50) not null
,peso numeric(18,4) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA LA RELACION ENTRE LAS TARAS Y TERMINALES PARA ESTABLECER EL PESO QUE RESULTA AL SUMAR AMBAS COSAS
create table terminaltara(id serial not null primary key
,id_terminal varchar(50) references terminales(id) not null
,id_tara varchar(50) references taras(id) not null
,id_contenedor integer references contenedores(id) not null
,peso_total numeric(18,4) not null
,cantidad_std numeric(18,4) not null
,semana integer not null
,status char(1) not null
,calidad char(1) not null
,maximo numeric(18,4) not null
,minimo numeric(18,4) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA ENLISTAR LOS MOVIMIENTOS DE LAS TERMINALES DENTRO DEL AREA DE SUPERMERCADO
create table movimientos(id bigserial not null primary key
,id_empleado varchar(50) references empleados(id) not null
,id_terminal_tara integer references terminaltara(id) not null
,id_maquina varchar(10) references maquinas(id) not null
,id_ruta integer references rutas(id) not null
,estimado numeric(18,4) not null
,estimadoReal numeric(18,4) null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
--TABLA PARA ENLISTAR LOS DESPERDICIOS (SCRAP) DE TERMINALES
create table residuos(id serial not null primary key
,terminaltara integer references terminaltara(id)
,scrap numeric(18,4) not null
,id_estado integer references estados(id) not null
,hora_registro time not null
,fecha_registro date not null);
-------------------------------------------------------------------------------------------------------
