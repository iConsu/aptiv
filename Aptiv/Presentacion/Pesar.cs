﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Presentacion.Properties;
using Negocio;
using Entidades;

namespace Presentacion
{
    public partial class Pesar : Form
    {
        private Validaciones ObjValidar = new Validaciones();
        private Intermedio ObjNegocio = new Intermedio();
        private PesarTerminal ObjPesarTerminal = new PesarTerminal();
        private Control[] Controles;
        private PictureBox[] Checks;
        private TextBox[] Textos;
        private byte Index;

        public Pesar()
        {
            InitializeComponent();
            Controles = new Control[] { ComboMaquina, ComboRuta, TextBoxPeso, TextBoxCantidad };
            Checks = new PictureBox[] { PictureBoxTerminal, PictureBoxTara, PictureBoxEmpleado, PictureBoxMaquina, PictureBoxRuta, PictureBoxPeso, PictureBoxCantidad };
            Textos = new TextBox[] { TextBoxTerminal, TextBoxTara, TextBoxEmpleado };
            ComboMaquina.DataSource = ObjNegocio.LlenarCombo(Combo.Maquina);
            ComboRuta.DataSource = ObjNegocio.LlenarCombo(Combo.Ruta);
            PictureBoxTerminal.Image = Resources.Unchecked;
            PictureBoxTara.Image = Resources.Unchecked;
            PictureBoxEmpleado.Image = Resources.Unchecked;
            ComboMaquina.SelectedIndex = -1;
            ComboRuta.SelectedIndex = -1;
            TextBoxPeso.Text = "0.00";
            TextBoxCantidad.Text = "0.00";
            TextBoxTerminal.Focus();
        }

        private bool ValidarCampos()
        {
            if (double.TryParse(TextBoxPeso.Text, out double Peso) && double.TryParse(TextBoxCantidad.Text, out double Cantidad) && ComboMaquina.SelectedIndex > -1 && ComboRuta.SelectedIndex > -1)
            {
                if (!string.IsNullOrEmpty(TextBoxDisableTerminal.Text) && !string.IsNullOrEmpty(TextBoxDisableTara.Text) && !string.IsNullOrEmpty(TextBoxDisableEmpleado.Text) && Peso > 0 && Cantidad > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void ControlesLeaveMetodo(object sender, EventArgs e)
        {
            for (; Index < Controles.Length; Index++)
                if (Controles[Index] == sender)
                    break;
            switch (Index)
            {
                case 0: //ComboMaquina
                    {
                        if(ComboMaquina.SelectedIndex != -1)
                        {
                            PictureBoxMaquina.Image = Resources.Checked;
                            ObjPesarTerminal.IdMaquina  = ComboMaquina.SelectedItem.ToString();
                        }
                        else
                        {
                            PictureBoxMaquina.Image = Resources.ErrorCheck;
                        }
                        break;
                    }
                case 1: //ComboRuta
                    {
                        if (ComboRuta.SelectedIndex != -1)
                        {
                            PictureBoxRuta.Image = Resources.Checked;
                            ObjPesarTerminal.IdRuta = ComboRuta.SelectedIndex;
                        }
                        else
                        {
                            PictureBoxRuta.Image = Resources.ErrorCheck;
                        }
                        break;
                    }
                case 2: //TextBoxPeso
                    {
                        if (TextBoxPeso.Text != "0.00" && double.TryParse(TextBoxPeso.Text, out double Peso))
                        {
                            ObjPesarTerminal.Peso = Peso;
                            PictureBoxPeso.Image = Resources.Checked;
                        }
                        else
                        {
                            PictureBoxPeso.Image = Resources.ErrorCheck;
                        }
                        break;
                    }
                case 3: //TextBoxCantidad
                    {
                        if (TextBoxCantidad.Text != "0.00" && double.TryParse(TextBoxCantidad.Text, out double Cantidad))
                        {
                            ObjPesarTerminal.Cantidad = Cantidad;
                            PictureBoxCantidad.Image = Resources.Checked;
                        }
                        else
                        {
                            PictureBoxCantidad.Image = Resources.ErrorCheck;
                        }
                        break;
                    }
                default:
                    throw new Exception("Excepción en Método: Pesar-CLM", new IndexOutOfRangeException());
            }
            Index = 0;
        } //Clave de Método: Pesar-CLM

        private void RegistrarClickMetodo(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                MessageBox.Show("Bien!");
                ButtonReiniciar.PerformClick();
            }
            else
            {
                MessageBox.Show("¡Mal!");
            }
        }

        private void ReiniciarClickMetodo(object sender, EventArgs e)
        {
            foreach (PictureBox Item in Checks) Item.Image = Resources.Unchecked;
            TextBoxTerminal.Text = string.Empty;
            TextBoxDisableTerminal.Text = string.Empty;
            TextBoxTara.Text = string.Empty;
            TextBoxDisableTara.Text = string.Empty;
            TextBoxEmpleado.Text = string.Empty;
            TextBoxDisableEmpleado.Text = string.Empty;
            ComboMaquina.SelectedIndex = -1;
            ComboRuta.SelectedIndex = -1;
            TextBoxPeso.Text = "0.00";
            TextBoxCantidad.Text = "0.00";
            TextBoxTerminal.Focus();
        }

        private void IngresarTerminalKey(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (ObjNegocio.ExisteId(Existe.Terminal, TextBoxTerminal.Text))
                {
                    TextBoxDisableTerminal.Text = TextBoxTerminal.Text;
                    PictureBoxTerminal.Image = Resources.Checked;
                    TextBoxTara.Focus();
                }
                else
                {
                    TextBoxTerminal.Text = string.Empty;
                    PictureBoxTerminal.Image = Resources.ErrorCheck;
                    TextBoxTerminal.Focus();
                }
            }
        } //Clave de Método: Pesar-ITK1

        private void IngresarTaraKey(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (ObjNegocio.ExisteId(Existe.Tara, TextBoxTara.Text))
                {
                    TextBoxDisableTara.Text = TextBoxTara.Text;
                    PictureBoxTara.Image = Resources.Checked;
                    TextBoxEmpleado.Focus();
                }
                else
                {
                    TextBoxTara.Text = string.Empty;
                    PictureBoxTara.Image = Resources.ErrorCheck;
                    TextBoxTara.Focus();
                }
            }
        } //Clave de Método: Pesar-ITK2

        private void IngresarEmpleadoKey(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (ObjNegocio.ExisteId(Existe.Empleado, TextBoxEmpleado.Text))
                {
                    TextBoxDisableEmpleado.Text = TextBoxEmpleado.Text;
                    PictureBoxEmpleado.Image = Resources.Checked;
                    ComboMaquina.Focus();
                }
                else
                {
                    TextBoxEmpleado.Text = string.Empty;
                    PictureBoxEmpleado.Image = Resources.ErrorCheck;
                    TextBoxEmpleado.Focus();
                }
            }
        } //Clave de Método: Pesar-IEK

        private void BotonIniciarLecturaClickMetodo(object sender, EventArgs e)
        {
            ButtonReiniciar.PerformClick();
            TextBoxTerminal.Focus();
        }
    }
}
