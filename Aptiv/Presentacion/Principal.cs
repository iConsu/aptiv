﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using Entidades;
using Negocio;

namespace Presentacion
{
    public partial class Principal : MaterialForm
    {
        #region InstanciasMenus
        private Inicio ObjInicio = new Inicio();
        private Pesar ObjPesar = new Pesar();
        private Localizar ObjLocalizar = new Localizar();
        #endregion

        private LogUser ObjLogeo;
        private readonly Entidad ObjEntidad = new Entidad();
        private Validaciones ObjValidar = new Validaciones();
        private ToolStripMenuItem[] Menus;
        private ToolStripMenuItem[] SubMenuConfiguracion;
        private byte Index;

        public Principal()
        {
            InitializeComponent();
            MaterialSkinManager SkinManager = MaterialSkinManager.Instance;
            SkinManager.AddFormToManage(this);
            SkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            SkinManager.ColorScheme = new ColorScheme(Primary.Orange800, Primary.Orange700, Primary.Orange600, Accent.Blue700, TextShade.BLACK);
            Menus = new ToolStripMenuItem[] { InicioMenu, PesarMenu, DevolverTerminalMenu, LocalizarMenu, ReportesMenu, InventarioMenu, ConfiguracionMenu, CerrarSesionMenu };
            SubMenuConfiguracion = new ToolStripMenuItem[] { EmpleadoSubMenu, TaraSubMenu, UnidadDeMedidaSubMenu };
        }

        public Principal(Entidad ObjEntidad)
        {
            InitializeComponent();
            MaterialSkinManager SkinManager = MaterialSkinManager.Instance;
            SkinManager.AddFormToManage(this);
            SkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            SkinManager.ColorScheme = new ColorScheme(Primary.Orange800, Primary.Orange700, Primary.Orange600, Accent.Blue700, TextShade.BLACK);
            Menus = new ToolStripMenuItem[] { InicioMenu, PesarMenu, DevolverTerminalMenu, LocalizarMenu, ReportesMenu, InventarioMenu, ConfiguracionMenu, CerrarSesionMenu };
            SubMenuConfiguracion = new ToolStripMenuItem[] { EmpleadoSubMenu, TaraSubMenu, UnidadDeMedidaSubMenu };

            this.ObjEntidad = ObjEntidad;
            ObjLogeo = new LogUser(ObjEntidad, Logeo.Cerrar);
            this.Size = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
            this.MinimumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
        } //Clave de Constructor: Principal-C1

        public void MostrarForm(Form Formulario)
        {
            Formulario.TopLevel = false;
            Formulario.Parent = PanelMenu;
            Formulario.Dock = DockStyle.Fill;
            if (!Formulario.Visible) Formulario.Show();
            Formulario.BringToFront();
        } //Clave de Método: Principal-MF

        private void ClickMethodMenu(object sender, EventArgs e)
        {
            for (; Index < Menus.Length; Index++)
                if (Menus[Index] == sender)
                    break;
            switch (Index)
            {
                case 0: //Inicio
                    {
                        try
                        {
                            MostrarForm(ObjInicio);
                        }
                        catch (NullReferenceException)
                        {
                            ObjInicio = new Inicio();
                            MostrarForm(ObjInicio);
                        }
                        break;
                    }
                case 1: //Pesar
                    {
                        try
                        {
                            MostrarForm(ObjPesar);
                        }
                        catch (NullReferenceException)
                        {
                            ObjPesar = new Pesar();
                            MostrarForm(ObjPesar);
                        }
                        break;
                    }
                case 3: //Localizar
                    {
                        try
                        {
                            if (ObjLocalizar.Visible)
                            {
                                ObjLocalizar.BringToFront();
                            }
                            else
                            {
                                ObjLocalizar.Show();
                                ObjLocalizar.BringToFront();
                            }
                        }
                        catch (ObjectDisposedException)
                        {
                            ObjLocalizar = new Localizar();
                            ObjLocalizar.Show();
                            ObjLocalizar.BringToFront();
                        }
                        break;
                    }
                case 7: //CerrarSesion
                    {
                        if (ObjLogeo.Visible)
                        {
                            ObjLogeo.Hide();
                        }
                        else
                        {
                            try
                            {
                                ObjLogeo.Show();
                                ObjLogeo.BringToFront();
                            }
                            catch (ObjectDisposedException)
                            {
                                ObjLogeo = new LogUser(ObjEntidad, Logeo.Cerrar);
                                ObjLogeo.Show();
                                ObjLogeo.BringToFront();
                            }
                        }
                        break;
                    }
                default:
                    throw new Exception("Excepción en Método: Principal-CMTSM", new IndexOutOfRangeException());
            }
            Index = 0;
        } //Clave de Método: Principal-CMTSM

        private void ClickMetodoSubMenu(object sender, EventArgs e)
        {

        } //Clave de Método: Principal-

        private void FormClosingMetodo(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                if (ObjLogeo.Visible)
                {
                    ObjLogeo.Hide();
                }
                else
                {
                    try
                    {
                        ObjLogeo.Show();
                        ObjLogeo.BringToFront();
                    }
                    catch (ObjectDisposedException)
                    {
                        ObjLogeo = new LogUser(ObjEntidad, Logeo.Cerrar);
                        ObjLogeo.Show();
                        ObjLogeo.BringToFront();
                    }
                }
            }
        } //Clave de Método: Principal-FCM
    }
}
