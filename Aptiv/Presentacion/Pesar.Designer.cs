﻿namespace Presentacion
{
    partial class Pesar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelTerminal = new System.Windows.Forms.Label();
            this.LabelEmpleado = new System.Windows.Forms.Label();
            this.LabelRuta = new System.Windows.Forms.Label();
            this.LabelPeso = new System.Windows.Forms.Label();
            this.LabelMaquina = new System.Windows.Forms.Label();
            this.ComboRuta = new System.Windows.Forms.ComboBox();
            this.ComboMaquina = new System.Windows.Forms.ComboBox();
            this.LabelCantidad = new System.Windows.Forms.Label();
            this.ButtonRegistrar = new System.Windows.Forms.Button();
            this.ButtonReiniciar = new System.Windows.Forms.Button();
            this.TextBoxPeso = new System.Windows.Forms.TextBox();
            this.TextBoxCantidad = new System.Windows.Forms.TextBox();
            this.PictureBoxEmpleado = new System.Windows.Forms.PictureBox();
            this.PictureBoxTerminal = new System.Windows.Forms.PictureBox();
            this.PictureBoxTara = new System.Windows.Forms.PictureBox();
            this.LabelTara = new System.Windows.Forms.Label();
            this.TextBoxTerminal = new System.Windows.Forms.TextBox();
            this.TextBoxTara = new System.Windows.Forms.TextBox();
            this.TextBoxEmpleado = new System.Windows.Forms.TextBox();
            this.ButtonIniciar = new System.Windows.Forms.Button();
            this.PictureBoxMaquina = new System.Windows.Forms.PictureBox();
            this.PictureBoxRuta = new System.Windows.Forms.PictureBox();
            this.PictureBoxPeso = new System.Windows.Forms.PictureBox();
            this.PictureBoxCantidad = new System.Windows.Forms.PictureBox();
            this.TextBoxDisableEmpleado = new System.Windows.Forms.TextBox();
            this.TextBoxDisableTara = new System.Windows.Forms.TextBox();
            this.TextBoxDisableTerminal = new System.Windows.Forms.TextBox();
            this.PanelOcultar = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTerminal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMaquina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRuta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxCantidad)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelTerminal
            // 
            this.LabelTerminal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelTerminal.AutoSize = true;
            this.LabelTerminal.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelTerminal.ForeColor = System.Drawing.Color.Black;
            this.LabelTerminal.Location = new System.Drawing.Point(562, 149);
            this.LabelTerminal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelTerminal.Name = "LabelTerminal";
            this.LabelTerminal.Size = new System.Drawing.Size(100, 30);
            this.LabelTerminal.TabIndex = 0;
            this.LabelTerminal.Text = "Terminal:";
            // 
            // LabelEmpleado
            // 
            this.LabelEmpleado.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelEmpleado.AutoSize = true;
            this.LabelEmpleado.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelEmpleado.ForeColor = System.Drawing.Color.Black;
            this.LabelEmpleado.Location = new System.Drawing.Point(446, 300);
            this.LabelEmpleado.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelEmpleado.Name = "LabelEmpleado";
            this.LabelEmpleado.Size = new System.Drawing.Size(216, 30);
            this.LabelEmpleado.TabIndex = 1;
            this.LabelEmpleado.Text = "Tarjeta de Empleado:";
            // 
            // LabelRuta
            // 
            this.LabelRuta.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelRuta.AutoSize = true;
            this.LabelRuta.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelRuta.ForeColor = System.Drawing.Color.Black;
            this.LabelRuta.Location = new System.Drawing.Point(601, 456);
            this.LabelRuta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelRuta.Name = "LabelRuta";
            this.LabelRuta.Size = new System.Drawing.Size(61, 30);
            this.LabelRuta.TabIndex = 2;
            this.LabelRuta.Text = "Ruta:";
            // 
            // LabelPeso
            // 
            this.LabelPeso.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelPeso.AutoSize = true;
            this.LabelPeso.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelPeso.ForeColor = System.Drawing.Color.Black;
            this.LabelPeso.Location = new System.Drawing.Point(491, 534);
            this.LabelPeso.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelPeso.Name = "LabelPeso";
            this.LabelPeso.Size = new System.Drawing.Size(171, 30);
            this.LabelPeso.TabIndex = 3;
            this.LabelPeso.Text = "Peso de báscula:";
            // 
            // LabelMaquina
            // 
            this.LabelMaquina.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelMaquina.AutoSize = true;
            this.LabelMaquina.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelMaquina.ForeColor = System.Drawing.Color.Black;
            this.LabelMaquina.Location = new System.Drawing.Point(452, 378);
            this.LabelMaquina.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelMaquina.Name = "LabelMaquina";
            this.LabelMaquina.Size = new System.Drawing.Size(210, 30);
            this.LabelMaquina.TabIndex = 4;
            this.LabelMaquina.Text = "Máquina de destino:";
            // 
            // ComboRuta
            // 
            this.ComboRuta.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ComboRuta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboRuta.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.ComboRuta.FormattingEnabled = true;
            this.ComboRuta.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18"});
            this.ComboRuta.Location = new System.Drawing.Point(666, 453);
            this.ComboRuta.Margin = new System.Windows.Forms.Padding(2);
            this.ComboRuta.Name = "ComboRuta";
            this.ComboRuta.Size = new System.Drawing.Size(367, 33);
            this.ComboRuta.TabIndex = 5;
            this.ComboRuta.Leave += new System.EventHandler(this.ControlesLeaveMetodo);
            // 
            // ComboMaquina
            // 
            this.ComboMaquina.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ComboMaquina.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboMaquina.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.ComboMaquina.FormattingEnabled = true;
            this.ComboMaquina.Location = new System.Drawing.Point(666, 375);
            this.ComboMaquina.Margin = new System.Windows.Forms.Padding(2);
            this.ComboMaquina.Name = "ComboMaquina";
            this.ComboMaquina.Size = new System.Drawing.Size(367, 33);
            this.ComboMaquina.TabIndex = 4;
            this.ComboMaquina.Leave += new System.EventHandler(this.ControlesLeaveMetodo);
            // 
            // LabelCantidad
            // 
            this.LabelCantidad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelCantidad.AutoSize = true;
            this.LabelCantidad.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelCantidad.ForeColor = System.Drawing.Color.Black;
            this.LabelCantidad.Location = new System.Drawing.Point(402, 614);
            this.LabelCantidad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelCantidad.Name = "LabelCantidad";
            this.LabelCantidad.Size = new System.Drawing.Size(260, 30);
            this.LabelCantidad.TabIndex = 15;
            this.LabelCantidad.Text = "Cantidad estimada a usar:";
            // 
            // ButtonRegistrar
            // 
            this.ButtonRegistrar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonRegistrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(124)))), ((int)(((byte)(0)))));
            this.ButtonRegistrar.FlatAppearance.BorderSize = 0;
            this.ButtonRegistrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            this.ButtonRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRegistrar.Font = new System.Drawing.Font("MS UI Gothic", 20F);
            this.ButtonRegistrar.ForeColor = System.Drawing.Color.White;
            this.ButtonRegistrar.Location = new System.Drawing.Point(666, 678);
            this.ButtonRegistrar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonRegistrar.Name = "ButtonRegistrar";
            this.ButtonRegistrar.Size = new System.Drawing.Size(367, 80);
            this.ButtonRegistrar.TabIndex = 8;
            this.ButtonRegistrar.Text = "REGISTRAR";
            this.ButtonRegistrar.UseVisualStyleBackColor = false;
            this.ButtonRegistrar.Click += new System.EventHandler(this.RegistrarClickMetodo);
            // 
            // ButtonReiniciar
            // 
            this.ButtonReiniciar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonReiniciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.ButtonReiniciar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(48)))), ((int)(((byte)(44)))));
            this.ButtonReiniciar.FlatAppearance.BorderSize = 2;
            this.ButtonReiniciar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(48)))), ((int)(((byte)(44)))));
            this.ButtonReiniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonReiniciar.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.ButtonReiniciar.ForeColor = System.Drawing.Color.White;
            this.ButtonReiniciar.Location = new System.Drawing.Point(11, 747);
            this.ButtonReiniciar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonReiniciar.Name = "ButtonReiniciar";
            this.ButtonReiniciar.Size = new System.Drawing.Size(271, 42);
            this.ButtonReiniciar.TabIndex = 6;
            this.ButtonReiniciar.TabStop = false;
            this.ButtonReiniciar.Text = "REINICIAR CAMPOS";
            this.ButtonReiniciar.UseVisualStyleBackColor = false;
            this.ButtonReiniciar.Click += new System.EventHandler(this.ReiniciarClickMetodo);
            // 
            // TextBoxPeso
            // 
            this.TextBoxPeso.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxPeso.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxPeso.Location = new System.Drawing.Point(666, 532);
            this.TextBoxPeso.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxPeso.Name = "TextBoxPeso";
            this.TextBoxPeso.Size = new System.Drawing.Size(367, 32);
            this.TextBoxPeso.TabIndex = 6;
            this.TextBoxPeso.Text = "0.00";
            this.TextBoxPeso.Leave += new System.EventHandler(this.ControlesLeaveMetodo);
            // 
            // TextBoxCantidad
            // 
            this.TextBoxCantidad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxCantidad.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxCantidad.Location = new System.Drawing.Point(666, 612);
            this.TextBoxCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxCantidad.Name = "TextBoxCantidad";
            this.TextBoxCantidad.Size = new System.Drawing.Size(367, 32);
            this.TextBoxCantidad.TabIndex = 7;
            this.TextBoxCantidad.Text = "0.00";
            this.TextBoxCantidad.Leave += new System.EventHandler(this.ControlesLeaveMetodo);
            // 
            // PictureBoxEmpleado
            // 
            this.PictureBoxEmpleado.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxEmpleado.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxEmpleado.Location = new System.Drawing.Point(1333, 282);
            this.PictureBoxEmpleado.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxEmpleado.Name = "PictureBoxEmpleado";
            this.PictureBoxEmpleado.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxEmpleado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxEmpleado.TabIndex = 6;
            this.PictureBoxEmpleado.TabStop = false;
            // 
            // PictureBoxTerminal
            // 
            this.PictureBoxTerminal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxTerminal.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxTerminal.Location = new System.Drawing.Point(1333, 131);
            this.PictureBoxTerminal.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxTerminal.Name = "PictureBoxTerminal";
            this.PictureBoxTerminal.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxTerminal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxTerminal.TabIndex = 5;
            this.PictureBoxTerminal.TabStop = false;
            // 
            // PictureBoxTara
            // 
            this.PictureBoxTara.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxTara.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxTara.Location = new System.Drawing.Point(1333, 203);
            this.PictureBoxTara.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxTara.Name = "PictureBoxTara";
            this.PictureBoxTara.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxTara.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxTara.TabIndex = 21;
            this.PictureBoxTara.TabStop = false;
            // 
            // LabelTara
            // 
            this.LabelTara.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelTara.AutoSize = true;
            this.LabelTara.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LabelTara.ForeColor = System.Drawing.Color.Black;
            this.LabelTara.Location = new System.Drawing.Point(604, 221);
            this.LabelTara.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelTara.Name = "LabelTara";
            this.LabelTara.Size = new System.Drawing.Size(58, 30);
            this.LabelTara.TabIndex = 20;
            this.LabelTara.Text = "Tara:";
            // 
            // TextBoxTerminal
            // 
            this.TextBoxTerminal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxTerminal.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxTerminal.Location = new System.Drawing.Point(1244, 90);
            this.TextBoxTerminal.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxTerminal.Name = "TextBoxTerminal";
            this.TextBoxTerminal.Size = new System.Drawing.Size(43, 32);
            this.TextBoxTerminal.TabIndex = 1;
            this.TextBoxTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IngresarTerminalKey);
            // 
            // TextBoxTara
            // 
            this.TextBoxTara.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxTara.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxTara.Location = new System.Drawing.Point(1291, 90);
            this.TextBoxTara.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxTara.Name = "TextBoxTara";
            this.TextBoxTara.Size = new System.Drawing.Size(43, 32);
            this.TextBoxTara.TabIndex = 2;
            this.TextBoxTara.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IngresarTaraKey);
            // 
            // TextBoxEmpleado
            // 
            this.TextBoxEmpleado.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxEmpleado.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxEmpleado.Location = new System.Drawing.Point(1338, 90);
            this.TextBoxEmpleado.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxEmpleado.Name = "TextBoxEmpleado";
            this.TextBoxEmpleado.Size = new System.Drawing.Size(43, 32);
            this.TextBoxEmpleado.TabIndex = 3;
            this.TextBoxEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IngresarEmpleadoKey);
            // 
            // ButtonIniciar
            // 
            this.ButtonIniciar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonIniciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.ButtonIniciar.FlatAppearance.BorderSize = 0;
            this.ButtonIniciar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(157)))), ((int)(((byte)(68)))));
            this.ButtonIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonIniciar.Font = new System.Drawing.Font("MS UI Gothic", 20F);
            this.ButtonIniciar.ForeColor = System.Drawing.Color.White;
            this.ButtonIniciar.Location = new System.Drawing.Point(666, 42);
            this.ButtonIniciar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonIniciar.Name = "ButtonIniciar";
            this.ButtonIniciar.Size = new System.Drawing.Size(367, 80);
            this.ButtonIniciar.TabIndex = 23;
            this.ButtonIniciar.Text = "INICIAR LECTURAS";
            this.ButtonIniciar.UseVisualStyleBackColor = false;
            this.ButtonIniciar.Click += new System.EventHandler(this.BotonIniciarLecturaClickMetodo);
            // 
            // PictureBoxMaquina
            // 
            this.PictureBoxMaquina.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxMaquina.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxMaquina.Location = new System.Drawing.Point(1333, 360);
            this.PictureBoxMaquina.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxMaquina.Name = "PictureBoxMaquina";
            this.PictureBoxMaquina.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxMaquina.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxMaquina.TabIndex = 26;
            this.PictureBoxMaquina.TabStop = false;
            // 
            // PictureBoxRuta
            // 
            this.PictureBoxRuta.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxRuta.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxRuta.Location = new System.Drawing.Point(1333, 438);
            this.PictureBoxRuta.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxRuta.Name = "PictureBoxRuta";
            this.PictureBoxRuta.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxRuta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxRuta.TabIndex = 27;
            this.PictureBoxRuta.TabStop = false;
            // 
            // PictureBoxPeso
            // 
            this.PictureBoxPeso.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxPeso.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxPeso.Location = new System.Drawing.Point(1333, 516);
            this.PictureBoxPeso.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxPeso.Name = "PictureBoxPeso";
            this.PictureBoxPeso.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxPeso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxPeso.TabIndex = 28;
            this.PictureBoxPeso.TabStop = false;
            // 
            // PictureBoxCantidad
            // 
            this.PictureBoxCantidad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxCantidad.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.PictureBoxCantidad.Location = new System.Drawing.Point(1333, 596);
            this.PictureBoxCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.PictureBoxCantidad.Name = "PictureBoxCantidad";
            this.PictureBoxCantidad.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxCantidad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxCantidad.TabIndex = 29;
            this.PictureBoxCantidad.TabStop = false;
            // 
            // TextBoxDisableEmpleado
            // 
            this.TextBoxDisableEmpleado.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxDisableEmpleado.Enabled = false;
            this.TextBoxDisableEmpleado.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxDisableEmpleado.Location = new System.Drawing.Point(666, 298);
            this.TextBoxDisableEmpleado.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxDisableEmpleado.Name = "TextBoxDisableEmpleado";
            this.TextBoxDisableEmpleado.Size = new System.Drawing.Size(367, 32);
            this.TextBoxDisableEmpleado.TabIndex = 100;
            this.TextBoxDisableEmpleado.TabStop = false;
            // 
            // TextBoxDisableTara
            // 
            this.TextBoxDisableTara.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxDisableTara.Enabled = false;
            this.TextBoxDisableTara.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxDisableTara.Location = new System.Drawing.Point(666, 219);
            this.TextBoxDisableTara.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxDisableTara.Name = "TextBoxDisableTara";
            this.TextBoxDisableTara.Size = new System.Drawing.Size(367, 32);
            this.TextBoxDisableTara.TabIndex = 100;
            this.TextBoxDisableTara.TabStop = false;
            // 
            // TextBoxDisableTerminal
            // 
            this.TextBoxDisableTerminal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxDisableTerminal.Enabled = false;
            this.TextBoxDisableTerminal.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxDisableTerminal.Location = new System.Drawing.Point(666, 147);
            this.TextBoxDisableTerminal.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxDisableTerminal.Name = "TextBoxDisableTerminal";
            this.TextBoxDisableTerminal.Size = new System.Drawing.Size(367, 32);
            this.TextBoxDisableTerminal.TabIndex = 100;
            this.TextBoxDisableTerminal.TabStop = false;
            // 
            // PanelOcultar
            // 
            this.PanelOcultar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PanelOcultar.BackColor = System.Drawing.Color.White;
            this.PanelOcultar.Location = new System.Drawing.Point(1244, 90);
            this.PanelOcultar.Name = "PanelOcultar";
            this.PanelOcultar.Size = new System.Drawing.Size(137, 32);
            this.PanelOcultar.TabIndex = 33;
            // 
            // Pesar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1435, 800);
            this.Controls.Add(this.PanelOcultar);
            this.Controls.Add(this.TextBoxDisableEmpleado);
            this.Controls.Add(this.TextBoxDisableTara);
            this.Controls.Add(this.TextBoxDisableTerminal);
            this.Controls.Add(this.PictureBoxCantidad);
            this.Controls.Add(this.PictureBoxPeso);
            this.Controls.Add(this.PictureBoxRuta);
            this.Controls.Add(this.PictureBoxMaquina);
            this.Controls.Add(this.ButtonIniciar);
            this.Controls.Add(this.PictureBoxTara);
            this.Controls.Add(this.PictureBoxTerminal);
            this.Controls.Add(this.PictureBoxEmpleado);
            this.Controls.Add(this.TextBoxEmpleado);
            this.Controls.Add(this.TextBoxTara);
            this.Controls.Add(this.TextBoxTerminal);
            this.Controls.Add(this.LabelTara);
            this.Controls.Add(this.TextBoxCantidad);
            this.Controls.Add(this.TextBoxPeso);
            this.Controls.Add(this.ButtonReiniciar);
            this.Controls.Add(this.ButtonRegistrar);
            this.Controls.Add(this.LabelCantidad);
            this.Controls.Add(this.ComboMaquina);
            this.Controls.Add(this.ComboRuta);
            this.Controls.Add(this.LabelMaquina);
            this.Controls.Add(this.LabelPeso);
            this.Controls.Add(this.LabelRuta);
            this.Controls.Add(this.LabelEmpleado);
            this.Controls.Add(this.LabelTerminal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Pesar";
            this.Text = "Pesar";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTerminal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMaquina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRuta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxCantidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelTerminal;
        private System.Windows.Forms.Label LabelEmpleado;
        private System.Windows.Forms.Label LabelRuta;
        private System.Windows.Forms.Label LabelPeso;
        private System.Windows.Forms.Label LabelMaquina;
        private System.Windows.Forms.PictureBox PictureBoxTerminal;
        private System.Windows.Forms.PictureBox PictureBoxEmpleado;
        private System.Windows.Forms.ComboBox ComboRuta;
        private System.Windows.Forms.ComboBox ComboMaquina;
        private System.Windows.Forms.Label LabelCantidad;
        private System.Windows.Forms.Button ButtonRegistrar;
        private System.Windows.Forms.Button ButtonReiniciar;
        private System.Windows.Forms.TextBox TextBoxPeso;
        private System.Windows.Forms.TextBox TextBoxCantidad;
        private System.Windows.Forms.PictureBox PictureBoxTara;
        private System.Windows.Forms.Label LabelTara;
        private System.Windows.Forms.TextBox TextBoxTerminal;
        private System.Windows.Forms.TextBox TextBoxTara;
        private System.Windows.Forms.TextBox TextBoxEmpleado;
        private System.Windows.Forms.Button ButtonIniciar;
        private System.Windows.Forms.PictureBox PictureBoxMaquina;
        private System.Windows.Forms.PictureBox PictureBoxRuta;
        private System.Windows.Forms.PictureBox PictureBoxPeso;
        private System.Windows.Forms.PictureBox PictureBoxCantidad;
        private System.Windows.Forms.TextBox TextBoxDisableEmpleado;
        private System.Windows.Forms.TextBox TextBoxDisableTara;
        private System.Windows.Forms.TextBox TextBoxDisableTerminal;
        private System.Windows.Forms.Panel PanelOcultar;
    }
}