﻿namespace Presentacion
{
    partial class Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.PictureBoxFacebook = new System.Windows.Forms.PictureBox();
            this.PictureBoxGitLab = new System.Windows.Forms.PictureBox();
            this.PictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.PanelBorde = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFacebook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxGitLab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(1082, 125);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(153, 18);
            this.materialLabel1.TabIndex = 3;
            this.materialLabel1.Text = "Proyecto en GitLab";
            // 
            // materialLabel2
            // 
            this.materialLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(1082, 193);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(215, 18);
            this.materialLabel2.TabIndex = 4;
            this.materialLabel2.Text = "Página Oficial de Facbeook";
            // 
            // PictureBoxFacebook
            // 
            this.PictureBoxFacebook.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxFacebook.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxFacebook.Image = global::Presentacion.Properties.Resources.Facebook;
            this.PictureBoxFacebook.Location = new System.Drawing.Point(1028, 163);
            this.PictureBoxFacebook.Name = "PictureBoxFacebook";
            this.PictureBoxFacebook.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxFacebook.TabIndex = 2;
            this.PictureBoxFacebook.TabStop = false;
            this.PictureBoxFacebook.Click += new System.EventHandler(this.LinksClickMethod);
            // 
            // PictureBoxGitLab
            // 
            this.PictureBoxGitLab.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxGitLab.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxGitLab.Image = global::Presentacion.Properties.Resources.GitLab;
            this.PictureBoxGitLab.Location = new System.Drawing.Point(1028, 95);
            this.PictureBoxGitLab.Name = "PictureBoxGitLab";
            this.PictureBoxGitLab.Size = new System.Drawing.Size(48, 48);
            this.PictureBoxGitLab.TabIndex = 1;
            this.PictureBoxGitLab.TabStop = false;
            this.PictureBoxGitLab.Click += new System.EventHandler(this.LinksClickMethod);
            // 
            // PictureBoxLogo
            // 
            this.PictureBoxLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PictureBoxLogo.Image = global::Presentacion.Properties.Resources.Logo;
            this.PictureBoxLogo.Location = new System.Drawing.Point(417, 100);
            this.PictureBoxLogo.MaximumSize = new System.Drawing.Size(600, 600);
            this.PictureBoxLogo.MinimumSize = new System.Drawing.Size(600, 600);
            this.PictureBoxLogo.Name = "PictureBoxLogo";
            this.PictureBoxLogo.Size = new System.Drawing.Size(600, 600);
            this.PictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxLogo.TabIndex = 0;
            this.PictureBoxLogo.TabStop = false;
            // 
            // PanelBorde
            // 
            this.PanelBorde.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PanelBorde.BackColor = System.Drawing.Color.Black;
            this.PanelBorde.Location = new System.Drawing.Point(412, 95);
            this.PanelBorde.Name = "PanelBorde";
            this.PanelBorde.Size = new System.Drawing.Size(610, 610);
            this.PanelBorde.TabIndex = 5;
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1435, 800);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.PictureBoxFacebook);
            this.Controls.Add(this.PictureBoxGitLab);
            this.Controls.Add(this.PictureBoxLogo);
            this.Controls.Add(this.PanelBorde);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Inicio";
            this.Text = "Inicio";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFacebook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxGitLab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBoxLogo;
        private System.Windows.Forms.PictureBox PictureBoxGitLab;
        private System.Windows.Forms.PictureBox PictureBoxFacebook;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.Panel PanelBorde;
    }
}